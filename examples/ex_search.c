/*
    ex_search.c
    Example to see if a domain on the specified TLD is already registered. For the RM-API.
    By Martin C (mchomenet.info). Dedicated to the Public Domain.
*/
#include <stdio.h>
#include <string.h>
#include <curl/curl.h>

char *URL="http://127.0.0.1/rm_api.cgi?"; /* change this */
char *user="OZTLD"; /* registrar userID */
char *userkey="1234567890abcdef"; /* registrar user key */
char requestURL[256];

static size_t parse_it(void *data, size_t size, size_t nmemb, void *userp)
{
    size_t realsize = size * nmemb;

    /* treat the data however you want now */
    printf("Raw Data: %s\n", data);
    printf("Parsed: ");
    switch(atoi(data))
    {
        case 0:
            printf("Domain available.\n");
            break;
        case 1:
            printf("Domain already registered.\n");
            break;
        case 255:
            printf("Server error. Please try again later.\n");
            break;
        default:
            printf("Unknown error.\n");
            break;
    }
    return realsize;
}

int main(int argc, char *argv[])
{
    CURL *curl;
    CURLcode res;
    char *tld, *domain;

    if(argc<3)
    {
        printf("More parameters needed:\n%s tld domain\n", argv[0]);
        return 1;
    }

    tld=argv[1];
    domain=argv[2];
    sprintf(requestURL, "%scmd=check&user=%s&userkey=%s&tld=%s&domain=%s", URL, user, userkey, tld, domain);
    #ifdef DEBUG
    printf("%s\n", requestURL);
    #endif
    curl=curl_easy_init();
    if(curl)
    {
        curl_easy_setopt(curl, CURLOPT_URL, requestURL);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, parse_it);
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "opennic-rmapi-client/1.0");
        res=curl_easy_perform(curl);
        if(res!= CURLE_OK)
        {
            fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
        }
        curl_easy_cleanup(curl);
    }
    return 0;
}
