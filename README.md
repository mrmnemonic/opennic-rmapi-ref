OpenNIC Remote Management Application Programming Interface - Reference Implementation.
Written 2012-2014 by Martin A. COLEMAN.

WORK-IN-PROGRESS, do not use unless you know what you are doing, though anyone with a bit of programming skill and determination should have no problem.

This project is hereby dedicated to the public domain.

This is not officially related to the OpenNIC Project in any way. This is an implementation of the RMAPI specification that will get added to as new ideas are proposed and explored for the API. Others may implement their own according to the specifications, this is just mine.

Will easily fit within the MUD4TLD system, if you run it.
