CC=gcc

rm_api: rm_api.c
	tcc -o rm_api.cgi rm_api.c ../../libs/qdecoder/src/qdecoder.a -lsqlite3

optimised: rm_api.c
	$(CC) -ansi -o rm_api.cgi rm_api.c ../../libs/qdecoder/src/qdecoder.a -lsqlite3

debug: rm_api.c
	$(CC) -g -ansi -o rm_api.cgi rm_api.c ../../libs/qdecoder/src/qdecoder.a -Wl,-export-dynamic
	
clean:
	rm rm_api.cgi
